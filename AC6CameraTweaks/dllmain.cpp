// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"

#define GLM_FORCE_SWIZZLE 1
#include <glm/glm.hpp>

#define MODUTILS_MACROS 1
#include <ModUtils.h>
#include <HookUtils.h>
#include <MathUtils.h>
#include <INIReader.h>

using namespace ModUtils;
using namespace MathUtils;
using namespace HookUtils;

#include <luajit/lua.hpp>

#include <set>
#include <unordered_map>
#include <memory>
#include <cstring>

#pragma comment(lib, "minhook.x64.lib")

#define CATIMPL(a, b) a ## b
#define CAT(a, b) CATIMPL(a, b)
#define SEQ(pref) CAT(pref, __COUNTER__)
#define PAD(size) char SEQ(_padding) [size];

template<typename T>
bool vector_contains(std::vector<T>& v, T needle)
{
    for (T elem : v)
    {
        if (elem == needle)
        {
            return true;
        }
    }
    return false;
}

template<typename T>
int vector_indexof(std::vector<T>& v, T needle)
{
    int i = 0;
    for (T elem : v)
    {
        if (elem == needle)
        {
            return i;
        }
        ++i;
    }
    return -1;
}

HMODULE g_hModule;
lua_State *g_Lua;

struct FConfig
{
    float FovMul = 1.0f;
    float distmul = 1.0f;
    float xoffset = 0.0f;
    float lockonOffsetMul = 1.f;
    float yOffset = 0.f;
    float PivotInterpMul = 1.f;
    float PivotInterpMulZ = 0.f;

    glm::vec3 Offset = { 0.f, 0, 0 };

    std::string DistLua = "d";

    void Read(std::string path)
    {
        INIReader ini(path);
        if (ini.ParseError() != 0)
        {
            ULog::Get().println("Cannot read config file.");
            return;
        }
        FovMul = ini.GetFloat("common", "fov-multiplier", FovMul);
        //distmul = ini.GetFloat("main", "distance-multiplier", distmul);
        yOffset = ini.GetFloat("common", "y-offset", yOffset);
        xoffset = ini.GetFloat("free-aim", "x-offset", xoffset);
        lockonOffsetMul = ini.GetFloat("lock-on", "x-offset-multiplier", lockonOffsetMul);
        PivotInterpMul = ini.GetFloat("pivot", "speed-multiplier", PivotInterpMul);
        PivotInterpMulZ = ini.GetFloat("pivot", "speed-multiplier-z", PivotInterpMulZ);
        DistLua = ini.Get("common", "distance", DistLua);
        //Offset = ini.GetVec("common", "freeaim-offset", Offset);

        if (!(PivotInterpMulZ > 0.f))
        {
            PivotInterpMulZ = PivotInterpMul;
        }
    }
} Config;

struct FCameraData
{
    // float RotationZ; // +1F0, when dashing
    // float interpZ; // +328
    // float interpXY; // +358, maybe just the x axis
    struct {
        PAD(0x20);
        void (*Tick)(void* pThis, float frametime, void*, void*);
    } *VT;
    PAD(sizeof(void*));
    glm::mat4 Transform;
    PAD(0x1E0 - 0x50);
    union {
        PAD(0x10);
        glm::vec2 RotationEuler; // x: pitch, y: yaw
    };
    PAD(0x215 - 0x1F0);
    bool bHasLockon; // +215
    PAD(0x328 - 0x216);
    float InterpSpeedZ;
    PAD(0x354 - (0x328 + 4));
    float InterpSpeedX;
};
FCameraData* pCameraData;

enum struct EScalarOffset : INT_PTR
{
    FOV = 0x50,
    X_OFFSET = 0x220,
    Y_OFFSET = 0x39C,
    MAX_DISTANCE = 0x2F4,
};

float LockonInterp;

/*
armoredcore6.exe+6C07CD - 49 8B 8F 30010000     - mov rcx,[r15+00000130]
armoredcore6.exe+6C07D4 - 4D 8B CD              - mov r9,r13
armoredcore6.exe+6C07D7 - 4D 8B C4              - mov r8,r12
armoredcore6.exe+6C07DA - 0F28 CF               - movaps xmm1,xmm7
armoredcore6.exe+6C07DD - 48 8B 01              - mov rax,[rcx]
armoredcore6.exe+6C07E0 - FF 50 20              - call qword ptr [rax+20]
armoredcore6.exe+6C07E3 - 49 8B 97 30010000     - mov rdx,[r15+00000130]
armoredcore6.exe+6C07EA - 49 8B CF              - mov rcx,r15
armoredcore6.exe+6C07ED - E8 4EF6FFFF           - call armoredcore6.exe+6BFE40
armoredcore6.exe+6C07F2 - E9 1A050000           - jmp armoredcore6.exe+6C0D11
armoredcore6.exe+6C07F7 - 49 8B 8F 38010000     - mov rcx,[r15+00000138]

NOTE: the game sometimes just rewrites this part of memory
*/

constexpr const char* const HOOK_PATTERN_VF_CAMERA_TICK = "4D 8B CD 4D 8B C4 0F28 CF 48 8B 01 FF 50 20 49 8B 97 30010000";
constexpr const int HOOK_OFFSET_CAMERA_TICK = 9;
constexpr const uint32_t HOOK_SIZE_CAMERA_TICK = 6;

extern "C" void PreHook_CameraTick();
extern "C" void Hook_CameraTick(FCameraData * pCamData, float frametime, LPVOID r8, LPVOID r9)
{
    pCameraData = pCamData;
    LockonInterp = InterpToF(LockonInterp, float(pCamData->bHasLockon), 2.f, frametime);
    pCamData->VT->Tick(pCamData, frametime, r8, r9);

    pCameraData = nullptr;
}

// interpolation function called for many scalar properties, fov, x offset, etc.
float (*tram_ScalarInterp)(LPVOID, float, float, BYTE, float, float);

float hk_ScalarInterp(LPVOID pIntermediates/*?*/, float target, float speed, BYTE r9/*is 0?*/, float f, float f1)
{
    if (pCameraData)
    {
        INT_PTR relAddress = UINT_PTR(pIntermediates) - UINT_PTR(pCameraData);

        switch (EScalarOffset(relAddress))
        {
        case EScalarOffset::FOV:
            target *= Config.FovMul;
            break;
        case EScalarOffset::X_OFFSET:
            //target += pCameraData->bHasLockon ? 0.f : Config.xoffset;
            break;
        case EScalarOffset::Y_OFFSET:
            //target += Config.yOffset;
            break;
        case EScalarOffset::MAX_DISTANCE:
            //target *= Config.distmul;
            break;
        }
    }

    float out = tram_ScalarInterp(pIntermediates, target, speed, r9, f, f1);
    return out;
}

// sometimes called right after the interp function
float (*tram_GetInterpResult)(LPVOID);
float hk_GetInterpResult(float* p)
{
    float result = tram_GetInterpResult(p);
    if (pCameraData)
    {
        INT_PTR relAddress = UINT_PTR(p) - UINT_PTR(pCameraData);

        switch (EScalarOffset(relAddress))
        {
        //case EScalarOffset::Y_OFFSET:
        //    return result + Config.yOffset; // sometimes the value is ignored
        //case EScalarOffset::FOV:
            //return out * Config.FovMul; // changing fov here will make the game freak out. wrong offset?
        case EScalarOffset::MAX_DISTANCE:
        {
            lua_pushnumber(g_Lua, result);
            lua_setglobal(g_Lua, "d");
            lua_pushnumber(g_Lua, pCameraData->RotationEuler.x);
            lua_setglobal(g_Lua, "pitch");
            std::string script = "out=(" + Config.DistLua + ")";
            if (luaL_loadbuffer(g_Lua, script.c_str(), script.size(), "dist") || lua_pcall(g_Lua, 0, 0, 0))
            {
                ULog::Get().dprintln("lua error: %s", lua_tostring(g_Lua, -1));
                lua_pop(g_Lua, 1);
            }
            lua_getglobal(g_Lua, "out");
            result = lua_tonumber(g_Lua, -1);
            lua_pop(g_Lua, 1);
            return result;
        }
        case EScalarOffset::X_OFFSET:
            return lerp(Config.xoffset, result * Config.lockonOffsetMul, LockonInterp);
            break;
        default:
            break;
        }
    }



    return result;
}


#if 0
glm::vec4* (*tram_PivotPos)(LPVOID, LPVOID, LPVOID, float, float);
glm::vec4* hk_PivotPos(LPVOID rcx, LPVOID rdx, LPVOID r8, float xOffset, float maxDist)
{
    glm::vec4* pOut = tram_PivotPos(rcx, rdx, r8, xOffset, maxDist);
    //pOut->y += Config.yOffset;
    return pOut;
}
#endif


/*
armoredcore6.exe+6CFF80 - 48 89 44 24 20        - mov [rsp+20],rax
armoredcore6.exe+6CFF85 - 4C 8D 8D C0110000     - lea r9,[rbp+000011C0]
armoredcore6.exe+6CFF8C - 4D 8B C5              - mov r8,r13
armoredcore6.exe+6CFF8F - 48 8D 95 600B0000     - lea rdx,[rbp+00000B60]
armoredcore6.exe+6CFF96 - 48 8B CF              - mov rcx,rdi
armoredcore6.exe+6CFF99 - E8 92660000           - call armoredcore6.exe+6D6630
armoredcore6.exe+6CFF9E - 0F28 00               - movaps xmm0,[rax]
armoredcore6.exe+6CFFA1 - 66 0F7F 85 C0120000   - movdqa [rbp+000012C0],xmm0
armoredcore6.exe+6CFFA9 - 0F28 9D F0000000      - movaps xmm3,[rbp+000000F0]
armoredcore6.exe+6CFFB0 - 0F29 5D 70            - movaps [rbp+70],xmm3
armoredcore6.exe+6CFFB4 - 0F57 F6               - xorps xmm6,xmm6
*/

constexpr static const char* const HOOK_PATTERN_FN_PIVOT_INTERP = "E8 ???????? 0F28 00 66 0F7F 85 ????0000 0F28 9D ????0000 0F29 5D ??";

glm::vec4* (*tram_PivotPosInterp)(LPVOID, LPVOID, LPVOID, LPVOID, LPVOID, LPVOID, float, float);
glm::vec4* hk_PivotPosInterp(LPVOID rcx, LPVOID rdx, LPVOID r8, LPVOID r9, LPVOID s0, LPVOID s1, float s2, float s3)
{
    if (pCameraData)
    {
        pCameraData->InterpSpeedX *= Config.PivotInterpMul;
        pCameraData->InterpSpeedZ *= Config.PivotInterpMulZ;
    }
    return tram_PivotPosInterp(rcx, rdx, r8, r9, s0, s1, s2, s3);
}


/*
armoredcore6.exe+6C6297 - 4C 8D 8D A0110000     - lea r9,[rbp+000011A0]
armoredcore6.exe+6C629E - 41 0F28 D5            - movaps xmm2,xmm13
armoredcore6.exe+6C62A2 - 48 8D 95 10140000     - lea rdx,[rbp+00001410]
armoredcore6.exe+6C62A9 - 48 8B CF              - mov rcx,rdi
armoredcore6.exe+6C62AC - E8 0F730000           - call armoredcore6.exe+6CD5C0
*/
constexpr char PATTERN_FN_PIVOT_OFFSET[] = "4C 8D 8D ????0000 41 0F28 D5 48 8D 95 ????0000 48 8B CF E8";

glm::vec4* (*tram_OffsetPivot)(LPVOID, LPVOID, float, LPVOID, LPVOID, LPVOID);
glm::vec4* hk_OffsetPivot(LPVOID rcx, LPVOID rdx, float xmm2, LPVOID r9 /*struct of vectors*/, glm::vec4* s0 /*global socket pos?*/, glm::vec3* s1 /*Y-plane normal?*/)
{
    glm::vec4* out = tram_OffsetPivot(rcx, rdx, xmm2, r9, s0, s1);

    out->y += Config.yOffset;

    //glm::mat4 rotation = glm::rotate(glm::mat4(1), pCameraData->RotationEuler.y, glm::vec3(0.f, 1.f, 0.f));
    //out->xyz += glm::vec3(rotation * glm::vec4(Config.Offset, 1.f));

    return out;
}

bool CheckHook(LPVOID pTarget)
{
    LPVOID hkJmpTarget = GetJumpTargetFar(GetJumpTargetNear(pTarget));
    if (hkJmpTarget == nullptr)
    {
        return false;
    }
    //ULog::Get().println("hook target %p", hkJmpTarget);
    return IsCurrentModuleAddress(hkJmpTarget, g_hModule);
}

std::unordered_map<std::string, std::unique_ptr<UHook>> Hooks;
//std::vector<UMinHook*> PersistChkHooks;

DWORD WINAPI ThreadCheckHook(LPVOID lpParam)
{
    for (;;)
    {
        if (Hooks.contains("CameraTick") && Hooks["CameraTick"]->IsEnabled())
        {
            auto& hook = Hooks["CameraTick"];
            if (auto& originalBytes = hook->GetOriginalBytes(); originalBytes.size())
            {
                std::vector<unsigned char> currentBytes(originalBytes.size());
                DWORD oldProtect, dummy;
                VirtualProtect(hook->GetHookTarget(), originalBytes.size(), PAGE_EXECUTE_READWRITE, &oldProtect);
                std::memcpy(currentBytes.data(), hook->GetHookTarget(), originalBytes.size());
                VirtualProtect(hook->GetHookTarget(), originalBytes.size(), oldProtect, &dummy);

                bool bytesMatch = std::memcmp(currentBytes.data(), originalBytes.data(), originalBytes.size()) == 0;
                if (bytesMatch)
                {
                    LOG_INFO << "CameraTick hook is overwritten, attempting to re-enable";
                    hook->Enable(true);
                }
            }
            else
            {
                assert(0);
            }
        }

        Sleep(1000);
    }

    return 0;
}

DWORD WINAPI MainThread(LPVOID lpParam)
{

    Sleep(3000);
    MH_Initialize();

    ULog::Get().println("config struct: %p", &Config);
    //Config.Read(TARGET_NAME ".ini");

    /* fov
        armoredcore6.exe+6D475E - 74 07                 - je armoredcore6.exe+6D4767
        armoredcore6.exe+6D4760 - E8 2B1F0100           - call armoredcore6.exe+6E6690
        armoredcore6.exe+6D4765 - EB 27                 - jmp armoredcore6.exe+6D478E
        armoredcore6.exe+6D4767 - F3 0F10 83 74080000   - movss xmm0,[rbx+00000874]
        armoredcore6.exe+6D476F - 44 0FB6 8B 78080000   - movzx r9d,byte ptr [rbx+00000878]
        armoredcore6.exe+6D4777 - F3 0F10 55 50         - movss xmm2,[rbp+50]
        armoredcore6.exe+6D477C - F3 44 0F11 5C 24 28   - movss [rsp+28],xmm11
        armoredcore6.exe+6D4783 - F3 0F11 44 24 20      - movss [rsp+20],xmm0
        armoredcore6.exe+6D4789 - E8 521F0100           - call armoredcore6.exe+6E66E0
        armoredcore6.exe+6D478E - 48 8B CE              - mov rcx,rsi
        armoredcore6.exe+6D4791 - E8 CA1E0100           - call armoredcore6.exe+
    */
    Hooks["ScalarInterp"] = std::make_unique<UMinHook>(
        "ScalarInterp",
        "F3 0F 10 55 50 F3 44 0F 11 5C 24 28 F3 0F 11 44 24 20 E8 ???????? 48 8B CE E8",
        18,
        &hk_ScalarInterp,
        (LPVOID*)(&tram_ScalarInterp)
    );

    Hooks["GetInterpResult"] = std::make_unique<UMinHook>(
        "GetInterpResult",
        "F3 0F 11 44 24 20 44 0F B6 8F 78 08 00 00 41 0F 28 D2 0F 28 CE 49 8B CE E8 ???????? 49 8B CE E8",
        32,
        &hk_GetInterpResult,
        (LPVOID*)(&tram_GetInterpResult)
    );

    Hooks["PivotOffset"] = std::make_unique<UMinHook>(
        "PivotOffset",
        PATTERN_FN_PIVOT_OFFSET,
        0x15,
        &hk_OffsetPivot,
        (LPVOID*)(&tram_OffsetPivot)
    );

    Hooks["PivotPosInterp"] = std::make_unique<UMinHook>(
        "PivotPosInterp",
        HOOK_PATTERN_FN_PIVOT_INTERP,
        &hk_PivotPosInterp,
        (LPVOID*)(&tram_PivotPosInterp)
    );

    Hooks["CameraTick"] = std::make_unique<UHookInline>(
        "CameraTick",
        HOOK_PATTERN_VF_CAMERA_TICK,
        HOOK_SIZE_CAMERA_TICK,
        PreHook_CameraTick,
        HOOK_OFFSET_CAMERA_TICK,
        false
    );

    CreateThread(0, 0, &ThreadCheckHook, 0, 0, 0);

    return 0;
}

DWORD WINAPI ThreadConfigRead(LPVOID lpParam)
{
    static constexpr size_t FILE_NOTIFY_BUFFER_SIZE = 1024;

    std::string filename = TARGET_NAME ".ini";
    std::wstring wfilename = WTEXT(TARGET_NAME) L".ini";

    Config.Read(filename);
    std::string modDir = GetDLLDirectory(g_hModule);
    while (1)
    {
        char notifybuf[FILE_NOTIFY_BUFFER_SIZE];
        DWORD numBytes;
        HANDLE hDir = CreateFileA(modDir.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);
        if (ReadDirectoryChangesW(hDir, &notifybuf, FILE_NOTIFY_BUFFER_SIZE, FALSE, FILE_NOTIFY_CHANGE_LAST_WRITE, &numBytes, 0, 0))
        {
            FILE_NOTIFY_INFORMATION* pNotify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(&notifybuf);
            if (numBytes)
            {
                while (1)
                {
                    std::wstring wsName(pNotify->FileName, pNotify->FileNameLength / sizeof(WCHAR));
                    if (wsName == wfilename)
                    {
                        std::string filename;
                        std::transform(wsName.begin(), wsName.end(), std::back_inserter(filename), [](wchar_t c) {
                            return (char)c;
                            });
                        ULog::Get().println("Config file changed");
                        Sleep(300); // workaround for some editors like vscode
                        Config.Read(modDir + "\\" + filename);
                    }
                    if (pNotify->NextEntryOffset == 0) break;
                    pNotify += pNotify->NextEntryOffset;
                }
            }
            else
            {
                ULog::Get().println("Couldn't setup config file watch");
                break;
            }
        }
    }

    return 0;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    if (ul_reason_for_call == DLL_PROCESS_ATTACH)
    {
        ULog::FileName = TARGET_NAME ".log";
        g_hModule = hModule;

#ifdef _DEBUG
        UHook::SetLogLevel(UHook::ELogLevel::VERBOSE);
#endif

        ULog& log = ULog::Get();
        HMODULE hMinhook = LoadLibraryA("minhook.x64.dll");
        if (hMinhook == NULL)
        {
            MessageBoxA(NULL, "Couldn't load MinHook.x64.dll, is it missing from the current directory?", TARGET_NAME, MB_OK);
            log.println("Couldn't load MinHook.x64.dll, aborting.");
            return FALSE;
        }
        g_Lua = lua_open();
        luaL_openlibs(g_Lua);

        DisableThreadLibraryCalls(hModule);
        CreateThread(0, 0, &MainThread, 0, 0, 0);
        CreateThread(0, 0, &ThreadConfigRead, 0, 0, 0);

    }
    if (ul_reason_for_call == DLL_PROCESS_DETACH)
    {
        lua_close(g_Lua);
        MH_Uninitialize();
    }
    return TRUE;
}

